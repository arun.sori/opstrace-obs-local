# Tiered Storages in ClickHouse

MergeTree table engine in ClickHouse supports tiered storage. See [docs](https://clickhouse.com/docs/en/engines/table-engines/mergetree-family/mergetree#table_engine-mergetree-multiple-volumes) for details on setup and further explanation.

Quoting the [doc](https://clickhouse.com/docs/en/engines/table-engines/mergetree-family/mergetree#table_engine-mergetree-multiple-volumes):

> MergeTree family table engines can store data on multiple block devices. For example, it can be useful when the data of a certain table are implicitly split into “hot” and “cold”. The most recent data is regularly requested but requires only a small amount of space. On the contrary, the fat-tailed historical data is requested rarely.

This is especially powerful when used with remote storage backends such as [S3](https://clickhouse.com/docs/en/engines/table-engines/mergetree-family/mergetree#table_engine-mergetree-s3).

It allows for storage policies which allows data to be on local disks for a period of time and then move it to object storage.

Example [configuration](https://clickhouse.com/docs/en/engines/table-engines/mergetree-family/mergetree#table_engine-mergetree-multiple-volumes_configure) can look like this:

```xml
<storage_configuration>
    <disks>
        <fast_ssd>
            <path>/mnt/fast_ssd/clickhouse/</path>
        </fast_ssd>
        <gcs>
            <support_batch_delete>false</support_batch_delete>
            <type>s3</type>
            <endpoint>https://storage.googleapis.com/${BUCKET_NAME}/${ROOT_FOLDER}/</endpoint>
            <access_key_id>${SERVICE_ACCOUNT_HMAC_KEY}</access_key_id>
            <secret_access_key>${SERVICE_ACCOUNT_HMAC_SECRET}</secret_access_key>
            <metadata_path>/var/lib/clickhouse/disks/gcs/</metadata_path>
        </gcs>
     ...
    </disks>
    ...
    <policies>

        <move_from_local_disks_to_gcs> <!-- policy name -->
            <volumes>
                <hot> <!-- volume name -->
                    <disk>fast_ssd</disk>  <!-- disk name -->
                </hot>
                <cold>
                    <disk>gcs</disk>
                </cold>
            </volumes>
            <move_factor>0.2</move_factor> <!-- move factor determines when to move data from hot volume to cold, see ClickHouse docs for more details-->
        </moving_from_ssd_to_hdd>
    ....
</storage_configuration>
```

In this storage policy, two volumes are defined `hot`  and `cold`. Once the `hot` volume is filled with occupancy of `disk_size * move_factor`, the data will be moved to GCS.

If this storage policy is not the default, tables can be created by attaching the storage policies. For example:

```sql
CREATE TABLE key_value_table (
    event_date Date,
    key String,
    value String,
) ENGINE = MergeTree
ORDER BY (key)
PARTITION BY toYYYYMM(event_date)
SETTINGS storage_policy = 'move_from_local_disks_to_gcs'
```

Note that in the above storage policy, the move happens implicitly. It is also possible to keep _hot_ data on local disks for a fixed period of time and then move them as _cold_.

This is possible with [Table TTLs](https://clickhouse.com/docs/en/engines/table-engines/mergetree-family/mergetree/#mergetree-table-ttl) which is also available with MergeTree table engine.

An [example](https://clickhouse.com/docs/en/guides/developer/ttl/#implementing-a-hotwarmcold-architecture) is available on ClickHouse docs which shows this feature in detail.

Similar approach can be taken for the example we discuss above.

First we would need to adjust the storage policy:

```xml
<storage_configuration>
    ...
    <policies>
        <local_disk_and_gcs> <!-- policy name -->
            <volumes>
                <hot> <!-- volume name -->
                    <disk>fast_ssd</disk>  <!-- disk name -->
                </hot>
                <cold>
                    <disk>gcs</disk>
                </cold>
            </volumes>
        </local_disk_and_gcs>
    ....
</storage_configuration>
```

We can then create the table as:

```sql
CREATE TABLE another_key_value_table (
    event_date Date,
    key String,
    value String,
) ENGINE = MergeTree
ORDER BY (key)
PARTITION BY toYYYYMM(event_date)
TTL 
    event_date TO VOLUME 'hot',
    event_date + INTERVAL 1 YEAR TO VOLUME 'cold'
SETTINGS storage_policy = 'local_disk_and_gcs';
```

This will create the table such as data older than 1 year (evaluated against `event_date` column) will be moved to GCS.

Such storage policy can be useful for tables which are append only (eg. audit events) and only most recent data is accessed frequently.
It is also possible to drop the data altogether which can be a regulatory requirement.

We don't mention modifying TTLs in this guide but that is possible as well. See ClickHouse [docs](https://clickhouse.com/docs/en/sql-reference/statements/alter/ttl/#modify-ttl) for details.
